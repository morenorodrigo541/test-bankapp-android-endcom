package com.rodrigomoreno.bankapp.RETROFIT;

import com.rodrigomoreno.bankapp.COMUN.Constantes;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BankClient {
    private static BankClient instance = null;
    private BankServices bankServices;
    private Retrofit retrofit;
    public BankClient() {



        retrofit = new Retrofit.Builder()
                .baseUrl(Constantes.API_BANKAPP_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        bankServices = retrofit.create(BankServices.class);
    }


    public static BankClient getInstance(){
        if (instance == null){
            instance = new BankClient();
        }
        return instance;
    }

    public BankServices getBankServices(){
        return bankServices;
    }
}
