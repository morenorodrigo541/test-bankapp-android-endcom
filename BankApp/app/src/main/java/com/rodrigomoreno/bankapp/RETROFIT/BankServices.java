package com.rodrigomoreno.bankapp.RETROFIT;

import com.rodrigomoreno.bankapp.model.Cuentum;
import com.rodrigomoreno.bankapp.model.RequestMovimientos;
import com.rodrigomoreno.bankapp.model.RequestTarjetas;
import com.rodrigomoreno.bankapp.model.Saldo;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;

public interface BankServices {

    @GET("cuenta")
    Call<Cuentum> getCuenta();

    @GET("saldos")
    Call<Saldo> getSaldo();

    @GET("tarjetas")
    Call<List<RequestTarjetas>> getTarjetas();

    @GET("movimientos")
    Call<List<RequestMovimientos>> getMovimientos();
}
