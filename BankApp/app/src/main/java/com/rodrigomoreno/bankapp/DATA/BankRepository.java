package com.rodrigomoreno.bankapp.DATA;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.rodrigomoreno.bankapp.COMUN.MyApp;
import com.rodrigomoreno.bankapp.RETROFIT.BankClient;
import com.rodrigomoreno.bankapp.RETROFIT.BankServices;
import com.rodrigomoreno.bankapp.UI.MyTarjetasRecyclerViewAdapter;
import com.rodrigomoreno.bankapp.model.RequestTarjetas;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankRepository {
    BankServices bankServices;
    BankClient bankClient;
    LiveData<List<RequestTarjetas>> allTarjetas;

    BankRepository(){
            bankClient = BankClient.getInstance();
            bankServices = bankClient.getBankServices();
            allTarjetas = getAllTarjetas();
    }

    public LiveData<List<RequestTarjetas>> getAllTarjetas(){
        final MutableLiveData<List<RequestTarjetas>> tarjeta = new MutableLiveData<>();
        Call<List<RequestTarjetas>> call = bankServices.getTarjetas();
        call.enqueue(new Callback<List<RequestTarjetas>>() {
            @Override
            public void onResponse(Call<List<RequestTarjetas>> call, Response<List<RequestTarjetas>> response) {
                if (response.isSuccessful()){
                    tarjeta.setValue(response.body());

                }else {
                    Toast.makeText(MyApp.getContext(), "Algo ha ido mal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<RequestTarjetas>> call, Throwable t) {
                Toast.makeText(MyApp.getContext(), "Error en la conexión", Toast.LENGTH_SHORT).show();
            }
        });
        return tarjeta;
    }
}
