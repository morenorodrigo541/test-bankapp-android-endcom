
package com.rodrigomoreno.bankapp.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSaldos {

    @SerializedName("saldos")
    @Expose
    private List<Saldo> saldos = null;

    public List<Saldo> getSaldos() {
        return saldos;
    }

    public void setSaldos(List<Saldo> saldos) {
        this.saldos = saldos;
    }

}
