package com.rodrigomoreno.bankapp.DATA;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.rodrigomoreno.bankapp.model.RequestTarjetas;

import java.util.List;

public class BankViewModel extends AndroidViewModel {

    private BankRepository repository;
    private LiveData<List<RequestTarjetas>> tarjetas;

    public BankViewModel(@NonNull Application application) {
        super(application);
        repository = new BankRepository();
        tarjetas = repository.getAllTarjetas();
    }

    public  LiveData<List<RequestTarjetas>> getTarjetas(){
        return tarjetas;
    }
}
