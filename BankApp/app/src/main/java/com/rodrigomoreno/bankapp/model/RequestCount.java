
package com.rodrigomoreno.bankapp.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class RequestCount {

    @SerializedName("cuenta")
    @Expose
    private List<Cuentum> cuenta = null;

    public List<Cuentum> getCuenta() {
        return cuenta;
    }

    public void setCuenta(List<Cuentum> cuenta) {
        this.cuenta = cuenta;
    }

}
