package com.rodrigomoreno.bankapp.UI;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rodrigomoreno.bankapp.R;
import com.rodrigomoreno.bankapp.model.RequestMovimientos;

import java.util.List;


public class MyMovimientosRecyclerViewAdapter extends RecyclerView.Adapter<MyMovimientosRecyclerViewAdapter.ViewHolder> {

    private List<RequestMovimientos> mValues;
    private Context ctx;

    public MyMovimientosRecyclerViewAdapter(Context context,List<RequestMovimientos> items) {
        mValues = items;
        ctx = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_movimiento, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvFecha.setText(holder.mItem.getFecha());
        holder.tvMonto.setText(holder.mItem.getMonto());
    }

    @Override
    public int getItemCount() {
        if (mValues != null)
            return mValues.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvFecha;
        public final TextView tvMonto;
        public RequestMovimientos mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvFecha = (TextView) view.findViewById(R.id.textViewFecha);
            tvMonto = (TextView) view.findViewById(R.id.textViewMonto);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvFecha.getText() + "'";
        }
    }
}