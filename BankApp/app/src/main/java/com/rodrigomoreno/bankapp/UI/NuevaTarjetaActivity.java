package com.rodrigomoreno.bankapp.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.rodrigomoreno.bankapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class NuevaTarjetaActivity extends AppCompatActivity implements View.OnClickListener {

    Button btnCancelar, btnAgregar;
    EditText numTarjeta,cuenta,issure;
    EditText nomTarjeta,marca,status,saldo;
    EditText tipoCuenta;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_tarjeta);
        btnCancelar = findViewById(R.id.buttonCancelar);
        btnAgregar = findViewById(R.id.buttonAgregarTarjeta);
        numTarjeta = findViewById(R.id.noTarjetaInput);
        cuenta = findViewById(R.id.cuentaInput);
        issure = findViewById(R.id.issureInput);
        nomTarjeta = findViewById(R.id.nombreInput);
        marca = findViewById(R.id.marcaInput);
        status = findViewById(R.id.statusInput);
        saldo = findViewById(R.id.saldoInput);
        tipoCuenta = findViewById(R.id.tipoInput);


        btnCancelar.setOnClickListener(this);
        btnAgregar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.buttonCancelar:
                Intent i = new Intent(this,MainActivity.class);
                startActivity(i);
                break;
            case R.id.buttonAgregarTarjeta:
                try {
                    agregarTarjeta();
                } catch (JSONException | FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }

    }

    private void agregarTarjeta() throws JSONException, IOException {
        JSONArray jsonArray = new JSONArray();
        JSONObject nvTarjeta;

        nvTarjeta = new JSONObject();
        nvTarjeta.put("noTarjeta",numTarjeta.getText().toString());
        nvTarjeta.put("cuenta",cuenta.getText().toString());
        nvTarjeta.put("issure",issure.getText().toString());
        nvTarjeta.put("nombreTarjeta",nomTarjeta.getText().toString());
        nvTarjeta.put("marca",marca.getText().toString());
        nvTarjeta.put("estado",status.getText().toString());
        nvTarjeta.put("saldo",saldo.getText().toString());
        nvTarjeta.put("tipodeCuenta",tipoCuenta.getText().toString());
        jsonArray.put(nvTarjeta);

        String archivo = jsonArray.toString();
        FileOutputStream nuevaFile = openFileOutput("NuevaTarjeta",MODE_PRIVATE);
        nuevaFile.write(archivo.getBytes());
        nuevaFile.close();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Nueva Tarjeta");
        builder.setMessage(jsonArray.toString());
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();

    }
}