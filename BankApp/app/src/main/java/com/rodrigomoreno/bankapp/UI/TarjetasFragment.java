package com.rodrigomoreno.bankapp.UI;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rodrigomoreno.bankapp.DATA.BankViewModel;
import com.rodrigomoreno.bankapp.R;
import com.rodrigomoreno.bankapp.RETROFIT.BankClient;
import com.rodrigomoreno.bankapp.RETROFIT.BankServices;
import com.rodrigomoreno.bankapp.model.RequestTarjetas;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TarjetasFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    RecyclerView recyclerView;
    MyTarjetasRecyclerViewAdapter adapter;
    List<RequestTarjetas> tarjetaList;
    BankViewModel bankViewModel;

    public TarjetasFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static TarjetasFragment newInstance(int columnCount) {
        TarjetasFragment fragment = new TarjetasFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bankViewModel = ViewModelProviders.of(getActivity())
                .get(BankViewModel.class);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tarjeta_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
             recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            adapter = new MyTarjetasRecyclerViewAdapter(
                    getActivity(),
                    tarjetaList
            );
            recyclerView.setAdapter(adapter);
            loadTarjetas();
        }
        return view;
    }



    private void loadTarjetas() {
        bankViewModel.getTarjetas().observe(getActivity(), new Observer<List<RequestTarjetas>>() {
            @Override
            public void onChanged(List<RequestTarjetas> requestTarjetas) {
                tarjetaList = requestTarjetas;
                adapter.setData(tarjetaList);
            }
        });

    }
}