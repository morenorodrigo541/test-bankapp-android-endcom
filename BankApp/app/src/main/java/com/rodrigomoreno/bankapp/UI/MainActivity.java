package com.rodrigomoreno.bankapp.UI;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.TextView;

import com.rodrigomoreno.bankapp.R;
import com.rodrigomoreno.bankapp.RETROFIT.BankClient;
import com.rodrigomoreno.bankapp.RETROFIT.BankServices;
import com.rodrigomoreno.bankapp.model.Cuentum;

import retrofit2.Call;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView tv_Nombre,tv_ultimaSesion;
    TextView tv_AgregarTarjeta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_AgregarTarjeta = findViewById(R.id.textViewToAgregar);
        tv_Nombre = findViewById(R.id.textViewNombre);
        tv_ultimaSesion = findViewById(R.id.textViewultima_sesion);

        tv_AgregarTarjeta.setOnClickListener(this);

    }



    @Override
    public void onClick(View v) {
        Intent i = new Intent(this,NuevaTarjetaActivity.class);
        startActivity(i);
    }
}