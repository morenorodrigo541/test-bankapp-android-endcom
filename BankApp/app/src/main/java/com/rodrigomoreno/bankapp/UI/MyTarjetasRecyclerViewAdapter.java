package com.rodrigomoreno.bankapp.UI;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rodrigomoreno.bankapp.R;
import com.rodrigomoreno.bankapp.model.RequestTarjetas;

import java.util.List;


public class MyTarjetasRecyclerViewAdapter extends RecyclerView.Adapter<MyTarjetasRecyclerViewAdapter.ViewHolder> {

    private Context ctx;
    private List<RequestTarjetas> mValues;

    public MyTarjetasRecyclerViewAdapter(Context context,List<RequestTarjetas> items) {
        mValues = items;
        ctx = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_tarjeta, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (mValues != null) {
            holder.mItem = mValues.get(position);

            holder.tvStatus.setText(holder.mItem.getEstado());
            holder.tvNomCliente.setText(holder.mItem.getNombre());
            holder.tvNumTarjeta.setText(holder.mItem.getTarjeta());
            holder.tvSaldo.setText(holder.mItem.getSaldo());
            holder.tvTipo.setText(holder.mItem.getTipo());
        }

    }

    public void setData(List<RequestTarjetas> tarjetasList){
        this.mValues = tarjetasList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mValues != null)
            return mValues.size();
        else return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvStatus;
        public final TextView tvNumTarjeta;
        public final TextView tvNomCliente;
        public final TextView tvTipo;
        public final TextView tvSaldo;
        public RequestTarjetas mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvStatus = (TextView) view.findViewById(R.id.textViewEstado);
            tvNumTarjeta = view.findViewById(R.id.textViewNumTarjeta);
            tvNomCliente = view.findViewById(R.id.textViewNombreCliente);
            tvTipo = view.findViewById(R.id.textViewTipo);
            tvSaldo = view.findViewById(R.id.textViewSaldoTarjeta);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvNomCliente.getText() + "'";
        }
    }
}